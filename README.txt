TWITTER BOOTSTRAP FRONT PAGE MODULE README

INSTRUCTIONS

(1) Download/turn on module.

(2) Configure the newly created block, editing title, sub-title and 
the URL the button links to in block configuration.

(3) If you only want the block to only appear on the front page, 
add <front> to the "includee only" field at the the bottom of 
the block configuration page.


DEPENDENCIES

This block is dependant on the Twitter Bootstrap Theme found 
at http://drupal.org/project/twitter_bootstrap. The theme 
has you install the Twitter Bootstrap Libraries. The libraries 
can also be installed manually from 
http://twitter.github.com/bootstrap/.
